<?php
require_once("OAuth/OAuth.php");
/**
 * Class responsible to get the OAuth autentification.
 */
class TakeIO
{
	private $consumerKey;
	private $consumerSecret;
	private $token;
	private $tokenSecret;

	function __construct($consumerKey,$consumerSecret,$token,$tokenSecret){
		$this->consumerKey=$consumerKey;
		$this->consumerSecret=$consumerSecret;
		$this->token=$token;
		$this->tokenSecret=$tokenSecret;
	}

	public function getCredential($http_method,$http_url){

		# Using OAuth 1.0 to get credential.
		$consumer = new OAuthConsumer($this->consumerKey, $this->consumerSecret);
		$access_token = new OAuthToken($this->token, $this->tokenSecret);
		$signature_method = new OAuthSignatureMethod_HMAC_SHA1();

		$r = OAuthRequest::from_consumer_and_token($consumer, $access_token, $http_method, $http_url);
		$r->sign_request($signature_method, $consumer, $access_token);

		$headers = array(
			"Content-Type: application/json; charset=utf-8",
			"Accept: application/json",
			$r->to_header());

		return $headers;
	}

  static function send($http_method,$http_url,$headers,$body) {

    #initialize a new session and return a cURL handle
    $session = curl_init();

    # Sets options on the given cURL session handle.
    curl_setopt($session, CURLOPT_CUSTOMREQUEST, $http_method);
    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($session, CURLOPT_URL, $http_url);
    curl_setopt($session, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($session, CURLOPT_FOLLOWLOCATION, true);

    #if method is POST or PUT sent together body of request
    if(strncmp($http_method,"POST", 4)==0 || strncmp($http_method,"PUT", 3)==0){
      curl_setopt($session, CURLOPT_POSTFIELDS, mb_convert_encoding(json_encode($body), 'utf-8'));
    }

    #execute the request
    $result = curl_exec($session);

    //just for debuggin
    $code = curl_getinfo($session, CURLINFO_HTTP_CODE);
    watchdog('sms_takeio', 'HTTP STATUS CODE: '.print_r($code, true));

    # close cURL handle
    curl_close($session);
    return $result;
  }

}

?>
